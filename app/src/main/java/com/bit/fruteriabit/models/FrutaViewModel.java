package com.bit.fruteriabit.models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.bit.fruteriabit.entities.Fruta;
import com.bit.fruteriabit.repositories.FrutaRepository;

import java.util.List;

public class FrutaViewModel extends AndroidViewModel {

    private FrutaRepository frutaRepository;
    private final LiveData<List<Fruta>> frutas;

    public FrutaViewModel (Application application){
            super(application);
            frutaRepository = new FrutaRepository(application);
            frutas = frutaRepository.getFrutas();
    }

    public LiveData<List<Fruta>> getFrutas(){
        return frutas;
    }

    public void insert (Fruta fruta) {
        frutaRepository.insert(fruta);
    }

}
