package com.bit.fruteriabit.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

//clase fruta
@Entity
public class Fruta {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String nombre;

    public Fruta(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Fruta() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Fruta{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
